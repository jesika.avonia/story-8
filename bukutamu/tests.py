from django.test import TestCase, Client
from .models import BukuTamu

# Create your tests here.
class Testing123(TestCase):
	def test_apakah_url_html_suatu_url_ada(self):
		response = Client().get('/suatu_url/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_url_formulir_ada(self):
		response = Client().get('/formulir/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_di_halaman_formulir_ada_text_BUKU_TAMU_dan_tombol_Kirim(self):
		response = Client().get('/formulir/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("BUKU TAMU", html_kembalian)
		self.assertIn("Kirim", html_kembalian)

	def test_apakah_di_halaman_formulir_ada_templatenya(self):
		response = Client().get('/formulir/')
		self.assertTemplateUsed(response, 'formulir.html')

	def test_apakah_url_hasil_ada(self):
		response = Client().get('/hasil/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_di_halaman_hasil_ada_templatenya(self):
		response = Client().get('/hasil/')
		html_kembalian = response.content.decode('utf8')
		self.assertIn("BUKU TAMU", html_kembalian)
		self.assertTemplateUsed(response, 'hasil.html')

	def test_apakah_sudah_ada_model_bukutamu(self):
		BukuTamu.objects.create(nama="INI NAMA", pesan="INI PESAN")
		hitung_berapa_bukunya = BukuTamu.objects.all().count()
		self.assertEquals(hitung_berapa_bukunya, 1)

	def test_apakah_di_halaman_hasil_sudah_menyimpan_data_dan_tampilkan_di_halaman_tsb(self):
		response = Client().post('/hasil/', {'nama': 'INI NAMA', 'pesan': 'INI PESAN'} )
		html_kembalian = response.content.decode('utf8')
		self.assertIn("BUKU TAMU", html_kembalian)
		self.assertIn("INI NAMA", html_kembalian)
		self.assertIn("INI PESAN", html_kembalian)



