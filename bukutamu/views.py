from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from .models import BukuTamu
import requests
import json


# Create your views here.

def fungsi_formulir(request):
	response = {}
	return render(request, 'formulir.html', response)

def fungsi_hasil(request):

	if request.method == 'POST':
		BukuTamu.objects.create(nama=request.POST['nama'], pesan = request.POST['pesan'])

	isi_data_buku_tamu = BukuTamu.objects.all()
	print(isi_data_buku_tamu)
	response = { 
		'data_list': isi_data_buku_tamu
	}
	return render(request, 'hasil.html', response)

def fungsi_suatu_url(request):
	response = {}
	return render(request, 'html_suatu_url.html', response)

def fungsi_data(request):
	url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
	ret = requests.get(url)
	print(ret.content)
	data = json.loads(ret.content)

	if not request.user.is_authenticated:
		i=0
		for x in data:
			del data[i]['volumeInfo']['imageLinks']['smallThumbnail']
			i=i+1
    		
	return JsonResponse(data, safe=False)